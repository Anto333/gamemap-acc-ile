public class Dot extends Coord{
	private String name;
	private char image; 
	private boolean show = true;

	public Dot(String name, char image, Coord coord, boolean show) {
		super(coord.getLat(), coord.getLong());
		this.name = name;
		this.image = image;
		this.show = show;
	}

	public void show() {
		show = true;
	}
	
	public void hide() {
		this.show = false;
		setCoord(0, 0);
	}

	public char getImage() {
		return image;
	}

	public boolean getShow() {
		return show;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void printInfo()
	{
		System.out.print(name + " (" + image + ")");
		if(show == true)
		{
			System.out.println(" Visible");
		}
		else
		{
			System.out.println(" Cache");
		}
		System.out.println("Latitude (i) : " + getLat() + " / Longitude (j) : " + getLong() + "\n");
	}
}
