class Gollum extends Enemy
{
    Gollum()
    {
        super("Gollum", 150, 150);
        Attack[] attacks = {
                new Attack("Chanson !", 15, 3),
                new Attack("Sournoiserie", 20, 5),
                new Attack("Appel d'Ungoliant", 30, 10),
                new Attack("Enigme", 40, 30) };
        setAttacks(attacks); 
    }

    @Override
    public void printskin()
    {
        System.out.println("           ___\r\n" + 
                           "         .';:;'.\r\n" + 
                           "        /_' _' /\\   __\r\n" + 
                           "        ;a/ e= J/-'\"  '.\r\n" + 
                           "        \\ ~_   (  -'  ( ;_ ,.\r\n" + 
                           "         L~\"'_.    -.  \\ ./  )\r\n" + 
                           "         ,'-' '-._  _;  )'   (\r\n" + 
                           "       .' .'   _.'\")  \\  \\(  |\r\n" + 
                           "      /  (  .-'   __\\{`', \\  |\r\n" + 
                           "     / .'  /  _.-'   \"  ; /  |\r\n" + 
                           "    / /    '-._'-,     / / \\ (\r\n" + 
                           " __/ (_    ,;' .-'    / /  /_'-._\r\n" + 
                           "`\"-'` ~`  ccc.'   __.','     \\j\\L\\\r\n" + 
                           "                 .='/|\\7      \r\n");

    }
}