import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Utils
{
    /*
     * Should not be instantiate.
     */
    private Utils()
    {
    }

    /**
     * Demande � lutilisateur de saisir un entier. R�p�te si l'utilisateur se trompe.
     * 
     * @param message    le message a afficher.
     * @param lowerBound valeur minimum acceptable.
     * @param upperBound valeur maximum acceptable.
     * @return la valeur saisie par l'utilisateur.
     */
    public static int promptInteger(String message, int lowerBound, int upperBound)
    {
        int result = promptInteger(message + "[" + lowerBound + "; " + upperBound + "] ");
        if (result < lowerBound || result > upperBound)
        {
            return promptInteger(message, lowerBound, upperBound);
        }
        else
        {
            return result;
        }
    }

    public static int promptInteger(String message)
    {
        String line = "";
        do
        {
            line = promptString(message);

        }
        while (!line.matches("\\s*\\d+\\s*")); // Checks that the input is an integer

        return Integer.parseInt(line.trim());
    }
    
    /** Return in a String object, a single char given by the user. */
    public static String promptChar(String message)
    {
        String line = "";
        do
        {
            line = promptString(message).trim();
        }
        while (line.length() != 1); // Ici si la chaine de char fait plus d'un char, on recommence.
        
        return line;
    }
    
    /** Asks the user to type a single letter parmis les lettres de la chaine choices. */
    public static String promptCharAmong(String message, String choices)
    {
        String line = "";
        do
        {
            line = promptChar(message).trim(); 
        }
        while (!choices.contains(line)); // Ici si le char saisi par l'user 
        // n'est pas contenu dans la chaine choice, on recommence.
        
        return line;
    }

    public static String promptString(String message)
    {
        try
        {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print(message + "  ");

            return br.readLine();
        }
        catch (IOException e)
        {
            // Means that the prompt is not available, the program should exit
            // UNREACHABLE
            return "";
        }
    }

    /** Generate random int between lower bound min and upper bound max. */
    public static int getRandomNumberInRange(int min, int max)
    {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
    
    public static boolean uniqCoord(int[] coords, int i, int j)
	{
		//La fonction compare
    	//les deux derniers �l�ments du tableau de positions (entr�es i et j)
		//aux �l�ments qui les pr�c�dent deux � deux.
		//Sert � �viter les doublons dans la g�n�ration des coordonn�es;
    	
    	boolean uniqPos = true;
		
		int max = i - 1;
		int l = 0;
		int m = 1;
		while(l <= (max - 1) && m <= max)
		{
			if (coords[i] == coords[l] && coords[j] == coords[m])
			{
				uniqPos = false;
			}
			l = l + 2;
			m = m + 2;
		}
		return uniqPos;
	}
    
    public static String upercase(String str1) {
		char[] str1_char = new char[str1.length()];
		int size = str1_char.length;
		int i = 0;
		while (i < size) {
			str1_char[i] = str1.charAt(i);
			i = i + 1;
		}

		i = 0;
		char[] str2_char = new char[size];

		while (i < size) {
			if (str1_char[i] >= 97 && str1_char[i] <= 122) {
				str2_char[i] = (char) (str1_char[i] - 32);
			} else {
				str2_char[i] = str1_char[i];
			}
			i = i + 1;
		}
		String str2 = new String(str2_char);
		return str2;
    }
}
