public class Attack
{
    private final String name;
    private final int dammage;
    private final int cost;
    
    public Attack(String name, int dammage, int cost)
    {
        super();
        this.name = name;
        this.dammage = dammage;
        this.cost = cost;
    }
    
    
    public void attack(Perso perso)
    {
        perso.lowerPv(dammage);
    }


    public String getName()
    {
        return name;
    }


    public int getDammage()
    {
        return dammage;
    }


    public int getCost()
    {
        return cost;
    }


    @Override
    public String toString()
    {
        return name + " (puissance = " + dammage + ", PA = " + cost + ")";
    }
}