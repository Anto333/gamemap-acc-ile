public class Castaway extends Perso
{
    protected Enemy enemy;

    Castaway(String name)
    {
        super(name, 250, 70);
        Attack[] attacks = {
    	        new Attack("Coup de pied", 10, 0),
    	        new Attack("Fronde", 15, 5),
    	        new Attack("Baton", 20, 10),
    	        new Attack("Coup de tete", 50, 20)
    	    	};
        setAttacks(attacks);
    }

    public Enemy getenemy()
    {
        return this.enemy;
    }

    public void levelup()
    
    {
        lvl = lvl + 1;
        pv = this.pv + 50;
        pa = this.pa + 40;
        System.out.println(" - Ce combat m'a galvanise ! Je me sens plus fort et plus beau !\n");
        System.out.println(Utils.upercase(getName()) + " A ATTEINT LE NIVEAU " + getLvl() + ".\n");
        System.out.println(Utils.upercase(getName()) + " A " + getPv() + " POINTS DE VIE.\n");
        System.out.println(Utils.upercase(getName()) + " A " + getPa() + " POINTS D'ACTION.\n");
    }

    public void selectAttack()
    {
        System.out.println(" - Quelle attaque est-ce que tu me conseilles contre " + enemy.getName() + "?\n");
        System.out.println("TAPE LE NUMERO DE L'ATTAQUE QUE TU VEUX UTILISER\n");
        
        int i = 0;
        int k = 0;
        while (i < attacks.length)
        {
        	if (this.pa >= attacks[i].getCost())
        	{
        		System.out.println("Attaque " + (i + 1) +" : " + attacks[i]);
        		k = k + 1;
        	}
        	i = i + 1;
        }
        
        System.out.print("CHOIX DE L'ATTAQUE ");
        int choice = Utils.promptInteger("", 1, (k));
        System.out.print(" : ");
        printAttackPlayer();
        Attack targetAttack = attacks[choice - 1];
        targetAttack.attack(enemy);
        lowerPa(targetAttack.getCost());
    }
    
    public void setenemy(Enemy enemy)
    {
        this.enemy = enemy;
    }
	
    private void printAttackPlayer()
	{
		System.out.println("\n");
    	System.out.print(
				   "   ,(   ,(   ,(   ,(   ,(   ,(   ,(   ,(   ,(   ,(   ,(   \n"
				+  "     `-'  `-'  `-'  `-'  `-'  `-'  `-'  `-'  `-'  `-'  `-'\n"
				+  "   ,(   ,(   ,(   ,(   ,(   ,(   ,(   ,(   ,(   ,(   ,(   \n"
				+  "     `-'  `-'  `-'  `-'  `-'  `-'  `-'  `-'  `-'  `-'  `-'\n");
	}
    
    public void printskin()
    {
        System.out.println(
        		"               ______      \r\n" + 
        		"             /`. -  ``:             \r\n" + 
        		"            |.` -:----::         C'est la carte de \r\n" + 
        		"          ./    :(@ \\(@\\         L'ILE DES ZERTES\n" + 
        		"           |  s.`    '/o                   \r\n" + 
        		"           \\..-     <``     `::`     \r\n" + 
        		"            ````s-:--'      -o+o`     \r\n" + 
        		"               d `.--     `.:/        \r\n" + 
        		"          ``.---.``.`os+/-..        \r\n" + 
        		"       sh.``            `y`          \r\n" + 
        		"    ``-hy/  d`.   `` `-s         \r\n" + 
        		"   :+/+:    d:--.-.-.:-s  Je dois aller a la rencontre       \r\n" + 
        		"   +--/      m-``.-.`.s-  des etres qui peuplent l'ile            \r\n" + 
        		" y/-o:`     d-  .-   s:               \r\n" + 
        		" `-/.      .y:::-:---oy.              \r\n" + 
        		"           yo./:-/.` -+              \r\n" + 
        		"          `y-` `o``` `:s:             \r\n" + 
        		"          /+`-  /:`  .`\\           \r\n" + 
        		"         /d/`o..-y:+/o-y`             \r\n" + 
        		"         :h :s:-  --`h`y-             \r\n" + 
        		"         s/.h`      .h`y.`            \r\n" + 
        		"        `h-/+       `y`s`             \r\n" + 
        		"        `so:       +///              \r\n" + 
        		"        -o+:o      `s.y.              \r\n" + 
        		"       o:+:o/     s-/+-:/o.         \r\n" + 
        		"       .-.`        `'':/o+'         \r\n");
    }
}
