abstract class Enemy extends Perso
{
    protected Castaway enemy;
    
    Enemy(String name, int pv, int pa) 
    {
        super(name, pv, pa);
    }
   
    public void randomAtk()
    {
    	int ranNum = 0;
        if (pa >= 30)
        	ranNum = Utils.getRandomNumberInRange(0, 3);
        else if (pa <30 && pa >=10)
            ranNum = Utils.getRandomNumberInRange(0, 2);
        else if (pa <10 && pa >=5)
            ranNum = Utils.getRandomNumberInRange(0, 1);
        else if (pa <5)
            ranNum = Utils.getRandomNumberInRange(0, 0);
        System.out.println( " - Cette vermine a plein de ressources ! \n");
        System.out.println(" - Que va-t-il faire ?");
        int i = 0;
        int k = 0;
        while (i < attacks.length)
        {
        	if (this.pa >= attacks[i].getCost())
        	{
        		System.out.println("Attaque " + (i + 1) +" : " + attacks[i]);
        		k = k + 1;
        	}
        	i = i + 1;
        }

        Attack targetAttack = attacks[ranNum];
        System.out.println("\n- Le mecreant ! Il utilise : " + attacks[ranNum] + ".");
        
        printAttackEnemy();
        targetAttack.attack(enemy);
        lowerPa(targetAttack.getCost());
    }

    public void setenemy(Castaway enemy)
    {
        this.enemy = enemy;
    }
    
	private void printAttackEnemy()
	{
		System.out.println("\n");
		System.out.print(
				   "   ___>_____>_____>_____>_____>_____>_____>_____>_____>___\n"
				+  "     ( )/  ( )/  ( )/  ( )/  ( )/  ( )/  ( )/  ( )/  ( )/ \n"
				+  "      (/    (/    (/    (/    (/    (/    (/    (/    (/  \n"
				+  "   _\\_____\\_____\\_____\\_____\\_____\\_____\\_____\\_____\\_____\\\n");
	}	
}