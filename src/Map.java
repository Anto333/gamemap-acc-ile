class Map {
	
	/**
	 * moves détermine la possibilité de bouger dans toutes les directions
	 * les boléens correspondent respectivement à : z, q, s, d;
	 */
	private boolean[] moves = { true, true, true, true };
	
	private int size;
	
	/*Dans l'ordre, les dots comprennent : 
	le castaway (dots[0])
	les 3 monstres (dots[1]) (dots[2]) (dots[3], dots[4])
	la bouteille (dots[5])
	la trap (dots[6])
	la coconut (dots[7])
	le boss (dots[8])
	voir le setDots() */
	
	public Dot[] dots;
	
	public Map(int size)
	{
		this.size = size;
		setDots();
	}

	public void move() {
		
		showmap();
		resetmoves();
		if (dots[0].getLat() == 1) // On est tout en haut
		{
			moves[0] = false;
		}
		if (dots[0].getLong() == 1) // On est tout à gauche
		{
			moves[1] = false;
		}
		if (dots[0].getLat() == size) // On est tout en bas
		{
			moves[2] = false;
		}
		if (dots[0].getLong() == size) // On est tout à droite
		{
			moves[3] = false;
		}
		moveprints();
		char choice = Utils.promptCharAmong("Choix : ", "zqsd").charAt(0);
		newcoords(choice);
	}

	private void moveprints() {
		
		System.out.println("VOUS ETES AUX COORDONNEES (" + dots[0].getLat() + " , " + dots[0].getLong() + ") DE LA CARTE. \n");
		if (moves[0] == false || moves[1] == false || moves[2] == false || moves[3] == false) {
			System.out.println("C'EST LE BORD DE LA CARTE :");
			if (moves[0] == false) {
				System.out.println("IMPOSSIBLE D'ALLER PLUS HAUT !");
			}
			if (moves[1] == false) {
				System.out.println("IMPOSSIBLE D'ALLER PLUS A GAUCHE !");
			}
			if (moves[2] == false) {
				System.out.println("IMPOSSIBLE D'ALLER PLUS BAS !");
			}
			if (moves[3] == false) {
				System.out.println("IMPOSSIBLE D'ALLER PLUS A DROITE !");
			}
		}
		System.out.println("OU VOULEZ-VOUS ALLER ?\n");
		if (moves[0] == true) {
			System.out.println("- z - en haut");
		}
		if (moves[1] == true) {
			System.out.println("- q - a gauche ");
		}
		if (moves[2] == true) {
			System.out.println("- s - en bas");
		}
		if (moves[3] == true) {
			System.out.println("- d - a droite");
		}
	}

	private void newcoords(char choice)
	{
		if (choice == 'z')
		{
			if (moves[0] == true)
			{
				dots[0].moveUp();
			}
			else
			{
				System.out.print("IL N'EST PAS POSSIBLE D'ALLER EN HAUT !\n");
				move();
			}
		}
		else if (choice == 'q')
		{
			if (moves[1] == true)
			{
				dots[0].moveLeft();
			}
			else
			{
				System.out.print("IL N'EST PAS POSSIBLE D'ALLER A GAUCHE !\n");
				move();
			}
		}
		else if (choice == 'd')
		{
			if (moves[3] == true)
			{
				dots[0].moveRight();
			}
			else
			{
				System.out.print("IL N'EST PAS POSSIBLE D'ALLER A DROITE !\n");
				move();
			}
		} 
		else if (choice == 's')
		{
			if (moves[2] == true)
			{
				dots[0].moveDown();
			}
			else
			{
				System.out.print("IL N'EST PAS POSSIBLE D'ALLER EN BAS !\n");
				move();
			}
		}
		else
		{
			System.out.println("DIRIGEZ-VOUS AVEC LES LETTRES DEMANDEES (ZQSD) !\n");
			move();
		}
	}

	private void resetmoves() {
		int i = 0;
		while (i < moves.length) {
			moves[i] = true;
			i = i + 1;
		}
	}
	
	private void showmap() {
			
			int i = 1;
			int k = 1;
			while (i <= size)
			{
				System.out.print(" -");
				while (k <= size)
				{
					System.out.print("------");
					k = k + 1;
				}
				System.out.print("\n");
				
				k = 1;
	
				int j = 1;
				while (j <= size)
				{
					Coord coord = new Coord(i, j);

					if (ifDot(coord, true) == true)
					{
						int pos = whatDot(coord, true);
						if (dots[pos].getShow() == true)
						{
							System.out.print(" | ");
							System.out.print(" " + dots[pos].getImage() + " ");
						}
						else
						{
							System.out.print(" | ");
							System.out.print("   ");
						}
					}
					else
					{
						System.out.print(" | ");
						System.out.print("   ");
					}
					j = j + 1;
				}
				System.out.println(" | ");
				System.out.print(" -");
				while (k <= size) {
					System.out.print("------");
					k = k + 1;
				}
				System.out.print("\n");
				k = 1;
				i = i + 1;
			}
		}

	private void setDots()
	{
		Coord[] coords = randomPos(size, 17); 
		int i = 0;
		Dot castaway = new Dot("Castaway", 'X', coords[i], true);
		i = i + 1;
		Dot monster1 = new Dot("Monstre", 'M', coords[i], true);
		i = i + 1;
		Dot monster2 = new Dot("Monstre", 'M', coords[i], true);
		i = i + 1;
		Dot monster3 = new Dot("Monstre", 'M', coords[i], true);
		i = i + 1;
		Dot monster4 = new Dot("Monstre", 'M', coords[i], true);
		i = i + 1;
		Dot bottle = new Dot("Bouteille", ' ', coords[i], true);
		i = i + 1;
		Dot trap = new Dot("Trappe", '?', coords[i], true);
		i = i + 1;
		Dot coconut = new Dot("Coconut", '?', coords[i], true);
		i = i + 1;
		Dot boss = new Dot("Boss", 'B', coords[i], false);
		i = i + 1;
		Dot tree = new Dot("Tree", 'Y', coords[i], true);
		i = i + 1;
		Dot tree2 = new Dot("Tree", 'Y', coords[i], true);
		i = i + 1;
		Dot tree3 = new Dot("Tree", 'Y', coords[i], true);
		i = i + 1;
		Dot tree4 = new Dot("Tree", 'Y', coords[i], true);
		i = i + 1;
		Dot tree5 = new Dot("Tree", 'Y', coords[i], true);
		i = i + 1;
		Dot tree6 = new Dot("Tree", 'Y', coords[i], true);
		i = i + 1;
		Dot tree7 = new Dot("Tree", 'Y', coords[i], true);
		i = i + 1;

		Dot[] dots = {castaway, monster1, monster2, monster3, monster4, bottle, trap, coconut, boss,
				      tree, tree2, tree3, tree4, tree5, tree6, tree7};
		this.dots = dots;
	}
	
	public static Coord[] randomPos(int size, int nbcoords) {
		// La fonction prend :
		// - la taille de la map
		// - le nombre de coordonnées à générer (soit le nombre de dots)

		int[] nb = new int[nbcoords * 2];

		int i = 0;
		while (i < nb.length) {
			nb[i] = Utils.getRandomNumberInRange(1, size);
			nb[i + 1] = Utils.getRandomNumberInRange(1, size);

			boolean uniq = Utils.uniqCoord(nb, i, (i + 1));
			if (uniq) {
				i = i + 2;
			}
		}
		
		Coord[] coords = new Coord[nbcoords];
		
		i = 0;
		int j = 0;
		while (i < coords.length)
		{
			coords[i] = new Coord(nb[j], nb[j + 1]);
			i = i + 1;
			j = j + 2;
		}
		return coords;
	}
	
	public int[] monstersRange()
	{
		int i = 0;
		int j = 0;
		while(dots[i].getName() != "Monstre")
		{
			i = i + 1;
			j = j + 1;
		}
		while(dots[j].getName() == "Monstre")
		{
			j = j + 1;
		}
		
		int[] range = {i, (j - 1)};
		return range;
	}
	
	public int nbMonsters()
	{
		int i = 0;
		int j = 0;
		while(dots[i].getName() != "Monstre")
		{
			i = i + 1;
			j = j + 1;
		}
		while(dots[j].getName() == "Monstre")
		{
			j = j + 1;
		}
		
		int nb = j - i;
		return nb;
	}
	
	public boolean ifDot(Coord coord, boolean all)
	{
		//La fonction prend des formes différentes selon le all
		//Si all est true, il s'agit de détecter la présence de n'importe quel dot sous les coordonnées indiquées
		//Si all est false, on détecte uniquement les monstres
		
		int i = 0;
		int j = 0;
		int max = dots.length;
		int size =  dots.length;
		int[] monstersRange = monstersRange();
		
		if (all == false) {
			j = monstersRange[0];
			max = monstersRange[1] + 1;
			size = nbMonsters();}
		
		Coord[] dotsCoord = new Coord[size];
		while(j < max)
		{
			dotsCoord[i] = dots[j].getCoord();
			j = j + 1;
			i = i + 1;
		}
		
		boolean presence = false;
		
		i = 0;
		while(i < dotsCoord.length)
		{
			if(coord.equals(dotsCoord[i]))
			{
				presence = true;
			}
			i =  i + 1;
		}
		return presence;
	}
	
	public int whatDot(Coord coord, boolean all)
	{
		//La fonction prend des formes différentes selon le all
		//Si all est true, il s'agit de détecter la présence de n'importe quel dot sous les coordonnées indiquées
		//Si all est false, on détecte uniquement les monstres
		
		int i = 0;
		int j = 0;
		int max = dots.length;
		int size =  dots.length;
		int[] monstersRange = monstersRange();
		
		if (all == false) {
			j = monstersRange[0];
			max = monstersRange[1] + 1;
			size = nbMonsters();}
		
		Coord[] dotsCoord = new Coord[size];
		while (j < max)
		{
			dotsCoord[i] = dots[j].getCoord();
			j = j + 1;
			i = i + 1;
		}
		
		i = 0;
		while(!coord.equals(dotsCoord[i]))
		{
			i = i + 1;
		}
		return i;
	}
	

}

