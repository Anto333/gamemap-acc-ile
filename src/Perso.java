abstract class Perso
{
    protected String name;
    protected int lvl;
    protected int pv;
    protected int pa;
    protected Attack[] attacks;

    Perso(String name, int pv, int pa)
    {
        this.name = name;
        lvl = 1;
        this.pv = pv;
        this.pa = pa;
    }
    
    public String getName()
    {
        return name;
    }

    public int getPv()
    {
        return pv;
    }
    
    public int getPa()
    {
        return pa;
    }

    public int getLvl()
    {
        return lvl;
    }

    protected void lowerPv(int dammage)
    {
        pv -= dammage;
        pv = pv < 0 ? 0 : pv;
        System.out.println("\n" + Utils.upercase(getName()) + " A MAINTENANT " + getPv() + " POINTS DE VIE.\n");
    }
    
    protected void lowerPa(int cost)
    {
        pa = this.pa - cost;
        System.out.println(Utils.upercase(getName()) + " A MAINTENANT " + getPa() + " POINTS D'ACTION\n");
    }
    
    protected void setAttacks(Attack[] attacks)
    {
        this.attacks = attacks;
    }
    
    public void printskin()
    {
    }
}
