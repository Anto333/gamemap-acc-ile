class Frog extends Enemy
{
    Frog()
    {
        super("The Mexican Staring Frog of Southern Sri Lanka", 90, 50);
        Attack[] attacks =
            {
                new Attack("Bond !", 15, 0),
                new Attack("Danse macabre", 20, 5),
                new Attack("Antoine Daniel", 30, 10),
                new Attack("Regard Hypnotique", 40, 30)
            };
        setAttacks(attacks);
    }

    @Override
    public void printskin()
    {
        System.out.println("\r\n" + 
                           "            ())                           _|_\r\n" + 
                           "'''''''''''(||)0                       _|_| |\r\n" + 
                           "                                       | || |  /\r\n" + 
                           "                                     \\ |_||_| /\r\n" + 
                           "                  (^)_(^)             \\ | /| /\r\n" + 
                           "       ~    ______(-----)_.__        ~ \\|/ |/\r\n" + 
                           " ~       _..   _  /_____\\  _ .._\r\n" + 
                           "        :     / \\/ |   | \\/ \\   `.\r\n" + 
                           "    ~    `___ |_\\__|___|__/_|     `.     ~\r\n" + 
                           "             /|\\  /|\\ /|\\  /|\\      ;\r\n" + 
                           "      .____.o o oo o o o oo o o     ;\r\n" + 
                           "   ~  :                           _.        ~\r\n" + 
                           "      :__                       __.\r\n" + 
                           "~       :_____         _____...\r\n" + 
                           "              :.......:             ~");   
    }
}