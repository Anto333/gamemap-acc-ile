public class Lile {
	public static void main(String[] args) {
		Game game = new Game();
		game.run();
	}

	static class Game 
	{
		public void run() {

			Castaway player = intro();
			player.printskin();
			Map map = new Map(7);
			Enemy[] Monsters = randomMonster();
			int toWin = map.nbMonsters();
			int j = 0;

			while (j < toWin) {
				// Cas où on est sur la même case qu'un monstre
				boolean presence = map.ifDot(map.dots[0].getCoord(), false);
				if (presence == true)
				{
					Monsters[j].printskin();
					fight(player, Monsters[j]);
					int pos = map.whatDot(map.dots[0].getCoord(), false);
					map.dots[pos + 1].hide();
					j = j + 1;
				}
				// Cas où on est sur la même case que la bouteille
				if (map.dots[0].getCoord().equals(map.dots[5].getCoord())) {
					printbottle();
					choixpopo(map, player);
					map.dots[5].hide();
				}
				// Cas où on est sur la même case que la trap
				if (map.dots[0].getCoord().equals(map.dots[6].getCoord())) {
					printtrap();
					trap(player);
					map.dots[6].hide();
				}
				// Cas où on est sur la même case que la coconut
				if (map.dots[0].getCoord().equals(map.dots[7].getCoord())) {
					printCoco();
					coconut(player);
					map.dots[7].hide();
				}
				map.move();
			}

			map.dots[8].show();
			System.out.println("\n****************************************************************\n");
			System.out.print(" - Un nouvel être est apparu sur la carte !\n" 
					+ " - Peut-être qu'il pourra m'aider !\n"
					+ " - Allons voir ce qu'il se passe dans ce coin-là ! \n");
			System.out.println("\n****************************************************************\n");
			// Cas où on est sur la même case que le boss
			while (!map.dots[0].getCoord().equals(map.dots[8].getCoord())) {
				map.move();
			}
			finalboss(player);
		}

		public Castaway intro() {
			printfly();
			System.out.println("\n - Bonjour, etes-vous confortablement assis avec le service de votre vol AirlowCost ?");
			System.out.println("\n - C'est un controle aleatoire en plein vol.");
			System.out.println("\n - Quel est votre nom ?\n");
			String name = Utils.promptString("   CHOIX");
			Castaway player = new Castaway(name);
			System.out.println("\n - Bien, tout est en ordre, " + player.getName() + ".");
			System.out.println("\n****************************************************************\n\n");
			System.out.println(" - Oh mon dieu !! L'helice est en feu !! On va se crasher !!\n");
			System.out.println("\n****************************************************************\n");
			printCrash();
			continues();
			printisland();
			System.out.println("\n" + player.getName() + " :  - J'ai survecu au crash de l'avion, je suis le seul...");
			System.out.println("\n - Il semble y avoir quelque chose qui se cache derriere cet arbre..."
					+ "\n \n - Un carton ! Et dans le carton, il y a une carte, toute propre !");
			continues();

			System.out.println("\n****************************************************************\n\n");
			return player;
		}

		public void continues() {
			System.out.println("\n****************************************************************\n");
			Utils.promptString("POUR CONTINUER, APPUYEZ SUR ENTREE : ");
			System.out.println("\n****************************************************************");
		}

		public void fight(Castaway player, Enemy enemy) {
			player.setenemy(enemy);
			enemy.setenemy(player);

			printCombat();
			System.out.println("\n" + Utils.upercase(player.getName()) + " A " + player.getPv() + " POINTS DE VIE.\n");
			System.out.println(Utils.upercase(enemy.getName()) + " A " + enemy.getPv() + " POINTS DE VIE.\n");

			System.out.print(" - A moi de jouer !\n\n");

			boolean i = true;
			while (player.getPv() > 0 && enemy.getPv() > 0) {
				if (i) {
					player.selectAttack();
					continues();
				} else {
					System.out.println(" - " + enemy.getName() + " m'attaque en retour ! Arghhhh !\n");
					enemy.randomAtk();
					continues();
				}
				i = !i;
			}

			if (enemy.getPv() <= 0) {
				System.out.println("BRAVOOOOOOOOOOOOOOOOOOOO !!!!!! C'EST GAGNE ! C'EST GAGNE !\n\n"
						+ Utils.upercase(player.getName()) + " A MIS " + Utils.upercase(enemy.getName())
						+ " PLUS BAS QUE TERRE. QUEL COMBAT EPOUSTOUFLANT !"
						+ "\n \n - C'etait de la gnognotte ! Mais quand meme j'ai eu super peur !!!\n");
				player.levelup();
				System.out.println("****************************************************************\n");
			} else {
				gameOver(player, enemy);
			}
		}

		public void finalboss(Castaway player) {
			Enemy boss = new Gollum();
			System.out.println("\n****************************************************************\n");
			System.out.println(
					" VOUS ENTREZ DANS UNE GROTTE ET UNE OMBRE SORT DU FOND. APRES UN PETIT MOMENT, VOUS ENTENDEZ UN CRI: \n"
							+ " " + boss.getName() + " - GOLLUM! GOLLUM! \n" 
							+ " " + boss.getName() + " - Bonjour " + player.getName() + ". Je t'attendais... Sauras-tu repondre a mes enigmes? \n");
			System.out.println("\n****************************************************************\n");
			boss.printskin();
			continues();
			fightboss(player, boss);
			printfinal(boss);
		}

		public void fightboss(Castaway player, Enemy boss) {
			String[] attacks = {
					"Un oeil dans un visage bleu \r\n" + "Vit un oeil dans un visage vert. \r\n"
							+ "Cet oeil-la ressemble a cet oeil-ci,\r\n" + "dit le premier oeil,\r\n"
							+ "Mais en un lieu bas,\r\n" + "Non pas en un lieu haut. ",
					"Cette chose toutes choses devore : \r\n" + "Oiseaux, betes, arbres, fleurs;\r\n"
							+ "Elle ronge le fer, mord l acier; \r\n" + "Reduit les dures pierres en poudre;\r\n"
							+ "Met a mort les rois, detruit les villes \r\n" + "Et rabat les hautes montagnes.\r\n",
					"On ne peut la voir, on ne peut la sentir,\r\n"
							+ "On ne peut l entendre, on ne peut la respirer.\r\n"
							+ "Elle s etend derriere les etoiles et sous les collines,\r\n"
							+ "Elle remplie les trous vides.\r\n" + "Elle vient d abord et suit apres.\r\n"
							+ "Elle termine la vie, tue le rire.  ",
					"Une boite sans charnieres, sans clef, sans couvercle :\r\n"
							+ "Pourtant a l interieur est cache un tresor dore. \r\n" };
			// int RaNum = Utils.getRandomNumberInRange(0, 3);;
			// System.out.println(attacks[RaNum]);
			int i = 0;

			while (i < attacks.length && player.getPv() > 0) {
				if (i == 0) {
					System.out.println("****************************************************************\n");
					System.out.println(attacks[0] + "\r\n");
					System.out.println("1 - Le soleil sur les marguerites ||     2 - L'eau                      \r\n"
							+ "3 - Aragorn !                     ||     4 - Le vent                    \r\n");

					int reponse = Utils.promptInteger("Votre Reponse", 1, 4);
					
					if (reponse == 1) {
						System.out.println("\n" + boss.getName()
								+ " - Bonne reponse il te reste encore 3 questions avant de me battre RrrRr ! \r\n");
						// boss.pv = boss.pv - 50;
						i = i + 1;
					} else {
						System.out.println(boss.getName() + " - Vous avez echoue ! \r\n");
						System.out.println(boss.getName()
								+ " - Ahaha, REFLECHISSEZ, GOLLUM ! GOLLUM ! \r\n");
						player.pv = player.pv - 30;
						System.out.println(boss.getName() + " VOUS A FAIT MAL! IL VOUS A ENLEVE 30 PV. IL VOUS RESTE "
								+ player.getPv() + " PV ");
						System.out.println("  ");
						i = 0;
					}
				} else if (i == 1) {
					System.out.println("****************************************************************\n");
					System.out.println(attacks[1] + "\r\n");
					System.out.println("1 - Antonin le tourmenteur   ||          2 - La guerre                 \r\n"
							+ "3 - Le temps                 ||          4 - La maladie                \r\n");

					int reponse = Utils.promptInteger("Votre Reponse", 1, 4);
					if (reponse == 3) {
						System.out.println("\n" +boss.getName()
								+ " - Bonne reponse il te reste encore 2 questions avant de me battre RrrRr !");
						// boss.pv = boss.pv - 50;
						i = i + 1;
					} else {
						System.out.println(boss.getName() + " - Vous avez echoue ! \r\n");
						System.out.println(boss.getName()
								+ " - Ahaha, REFLECHISSEZ, GOLLUM ! GOLLUM ! \r\n");
						player.pv = player.pv - 30;
						System.out.println(boss.getName() + " VOUS A FAIT MAL! IL VOUS A ENLEVE 30 PV. IL VOUS RESTE "
								+ player.getPv() + " PV ");
						System.out.println("  ");
						i = 1;
					}
				} else if (i == 2) {
					System.out.println("****************************************************************\n");
					System.out.println(attacks[2] + "\r\n");
					System.out.println("1 - L'odeur du temps         ||          2 - L'obscurite              \r\n"
							+ "3 - La vie                   ||          4 - Black Widow alias Cecile  \r\n");

					int reponse = Utils.promptInteger("Votre Reponse", 1, 4);
					if (reponse == 2) {
						System.out.println("\n" + boss.getName()
								+ " - Bonne reponse il te reste encore 1 question avant de me battre RrrRr !");
						// boss.pv = boss.pv - 50;
						i = i + 1;
					} else {
						System.out.println(boss.getName() + " - Vous avez echoue ! \r\n");
						System.out.println(boss.getName()
								+ " - Ahaha, REFLECHISSEZ, GOLLUM ! GOLLUM ! \r\n");
						player.pv = player.pv - 30;
						System.out.println(boss.getName() + " VOUS A FAIT MAL! IL VOUS A ENLEVE 30 PV. IL VOUS RESTE "
								+ player.getPv() + " PV ");
						System.out.println("  ");
						i = 2;
					}
				} else if (i == 3) {
					System.out.println("****************************************************************\n");
					System.out.println(attacks[3] + "\r\n");
					System.out.println("1 - La Reponse D             ||          2 - Un Volcan        \r\n"
							+ "3 - Un oeuf                ||          4 - l'Atome          \r\n");

					int reponse = Utils.promptInteger("Votre Reponse", 1, 4);
					if (reponse == 3) {
						System.out.println("\n" + boss.getName() + " - Vous avez reussi ! je vais tenir parole... \r\n"
								+ " - Venez je vais vous conduire au village le plus proche");
						// boss.pv = boss.pv - 50;
						i = i + 1;
					} else {
						System.out.println(boss.getName() + " - Vous avez echoue ! \r\n");
						System.out.println(boss.getName()
								+ " - Ahaha, REFLECHISSEZ, GOLLUM ! GOLLUM ! \r\n");
						player.pv = player.pv - 30;
						System.out.println(boss.getName() + " VOUS A FAIT MAL! IL VOUS A ENLEVE 30 PV. IL VOUS RESTE "
								+ player.getPv() + " PV ");
						System.out.println("  ");
						i = 3;
					}
				}
			}
			if(player.getPv() <= 0)
			{
				gameOver(player, boss);
			}
		}

		public void gameOver(Castaway player, Enemy enemy) {

			System.out.println("\n****************************************************************\n\n"
					+ enemy.getName() + " NE VOUS A PAS RESPECTE. \n\n" + player.getName()
					+ " A SUCCOMBE A SA DERNIERE ATTAQUE.\n\n" + "L'ESCALE SUR L'ILE DESERTE S'ARRETE ICI\n\n"
					+ "GAME OVER\n\n" + "****************************************************************");

			System.out.println("\n\nON SE FAIT UNE NOUVELLE PARTIE ?\n");
			System.out.println("1 - Oui\n");
			System.out.println("2 - Non\n");

			int choice = Utils.promptInteger("CHOIX", 1, 2);

			if (choice == 1) {
				Game game = new Game();
				game.run();
			} else if (choice == 2) {
				System.exit(0);
			}

		}

		public Enemy randomEnemy()

		{
			// Enemy[] monster = { new Frog(), new Centaur(), new Solid_Snake(), new
			// Unicorn() };
			// Enemy enemychosen = monster[ranEnemy];
			Enemy enemychosen = null;
			int ranEnemy = Utils.getRandomNumberInRange(0, 3);
			if (ranEnemy == 0) {
				enemychosen = new Frog();
			}
			if (ranEnemy == 1) {
				enemychosen = new Centaur();
			}
			if (ranEnemy == 2) {
				enemychosen = new unicorn();
			}
			if (ranEnemy == 3) {
				enemychosen = new Solid_Snake();
			}
			return enemychosen;
		}

		public Enemy[] randomMonster() {
			Enemy one = randomEnemy();
			Enemy two = randomEnemy();
			while (one.getName().equals(two.getName()))
			{
				two = randomEnemy();
			}
			Enemy three = randomEnemy();
			while (three.getName().equals(two.getName())
					| three.getName().equals(one.getName()))
			{
				three = randomEnemy();
			}
			Enemy four = randomEnemy();
			
			while (four.getName().equals( two.getName() )
					| four.getName().equals( one.getName() ) | four.getName().equals( three.getName() ))
			{
				four = randomEnemy();
			}

			Enemy[] Monster = { one, two, three, four };
			return Monster;
		}

		public void trap(Castaway player)

		{
			System.out.println(" VOUS ETES TOMBE DANS LE PIEGE DE L AMIRAL!!");
			System.out.println(" VOUS AVEZ PERDU 30 PV");
			player.pv = player.pv - 30;
			System.out.println(" IL VOUS RESTE " + player.getPv()
					+ " PV!! VOUS ETES UN PEU UN NOOB... FAITES ATTENTION LA PROCHAINE FOIS! :'(");
			continues();
		}

		public void choixpopo(Map map, Castaway player) {
			System.out.println(
					"- Genial! Une bouteille qui traine par terre sur une ile deserte, pleine de monstres et de danger! \n" 
				  + "- Ca m'a l'air super safe de la boire dis-donc!");
			System.out.println("- Tapez 1 - VOUS LA BUVEZ! YOOOLOOOOOOOOOOO!");
			System.out.println("- Tapez 2 - VOUS LA LAISSEZ... PETITE NATURE!");
			int choix = Utils.promptInteger("Choix : ", 1, 2);
			if (choix == 1) {
				System.out.println("- Holala! c'etait une super popo de sante aromatisee anis et caramel !\n"
						+ "- Quelle chance! Mes PV etaient de " + player.getPv());
				player.pv = player.pv + 30;
				System.out.print("- Et ils sont maintenant de " + player.getPv());
				continues();
			}
			if (choix == 2) {
				System.out.println("DOMMAGE... ON AVAIT CODE UN TRUC GENIAL SI LE CHOIX ETAIT 1 :'(");
				System.out.println(
						"Il ne s'est absolument rien passe dis-donc! Il ne peut plus rien m'arrivez d'affreux maintenant!");
				continues();
			}
		}

		public void coconut(Castaway player)

		{
			System.out.println(" VOUS TROUVEZ UNE SUPERBE NOIX DE COCO! VOUS MANGEZ VOUS REMET SUR PIED.");
			player.pv = player.pv + 30;
			player.pa = player.pa + 40;
			System.out.println(" VOUS AVEZ GAGNE 30 PV AINSI QUE 40 PA");
			System.out.println(" "+
					Utils.upercase(player.getName()) + " A MAINTENANT " + player.getPv() + " PV AINSI QUE " + player.getPa() + " PA.");
			continues();
		}

		public void printCrash() {
			System.out.print("                             ____                             \n"
					+ "                      __,-~~/~    `---.                        \n"
					+ "                    _/_,---(      ,    )                     \n"
					+ "                __ /  BOUM   <    /   )  \\___              \n"
					+ " - ------===;;;'====------------------===;;;===----- -  -   - \n"
					+ "_________________ \\/  ~\"~\"~\"~\"~\"~\\~\"~)~\"/______________________   \n"
					+ "     ~          ~ (_ (   \\  (     >    \\)                 \n"
					+ "                   \\_( _ <  CRASH   >_>'                   \n"
					+ "                      ~ `-i' ::>|--\"      ~~               \n"
					+ "           ~       ___---``I;|.|.|``---___          ~      \n"
					+ "      ~           /     <|i::|i|`.        \\.._           \n"
					+ "           ~~   /      (` ^'\"`-' \")           ]  ~~      \n"
					+ "               [                            |               \n"
					+ "       ~        [       _\\/_                 /        ~        \n"
					+ "              ~  \\_     /o\\\\\\/_           _-''              \n"
					+ "                    |    | |/o\\    . __.;_/   ~              \n"
					+ "               ~~    \\_  |   |    /''             ~           \n"
					+ "                        ``..-|---'            ~               \n");
		}

		public void printfinal(Enemy boss) {
			System.out.println("    .                  .-.    .  _   *     _   .\r\n"
					+ "           *          /   \\     ((       _/ \\       *    .\r\n"
					+ "         _    .   .--'\\/\\_ \\     `      /    \\  *    ___\r\n"
					+ "     *  / \\_    _/ ^      \\/\\'__        /\\/\\  /\\  __/   \\ *\r\n"
					+ "       /    \\  /    .'   _/  /  \\  *' /    \\/  \\/ .`'\\_/\\   .\r\n"
					+ "  .   /\\/\\  /\\/ :' __  ^/  ^/    `--./.'  ^  `-.\\ _    _:\\ _\r\n"
					+ "     /    \\/  \\  _/  \\-' __/.' ^ _   \\_   .'\\   _/ \\ .  __/ \\\r\n"
					+ "   /\\  .-   `. \\/     \\ / -.   _/ \\ -. `_/   \\ /    `._/  ^  \\\r\n"
					+ "  /  `-.__ ^   / .-'.--'    . /    `--./ .-'  `-.  `-. `.  -  `.\r\n"
					+ "@/        `.  / /      `-.   /  .-'   / .   .'   \\    \\  \\  .-  \\%\r\n"
					+ "@&8jgs@@%% @)&@&(88&@.-_=_-=_-=_-=_-=_.8@% &@&&8(8%@%8)(8@%8 8%@)%\r\n"
					+ "@88:::&(&8&&8:::::%&`.~-_~~-~~_~-~_~-~~=.'@(&%::::%@8&8)::&#@8::::\r\n"
					+ "`::::::8%@@%:::::@%&8:`.=~~-.~~-.~~=..~'8::::::::&@8:::::&8:::::'\r\n"
					+ " `::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.'");
			System.out.println("------------------------------------------------------------------------");
			System.out.println(
					"---------" + Utils.upercase(boss.getName()) + " VOUS EMENE-T-IL EN LIEU SUR ? -------------");
			System.out.println("------------------------------------------------------------------------");

			System.out.println("                                  ,----.__                         |\r\n"
					+ "                                ,'        `.                       |\r\n"
					+ "                            _  /            :                      ,-.\r\n"
					+ "                           |.`:              :                    /  -\r\n"
					+ "            ,'''''-._      | )               :                 _.'  --\r\n"
					+ "           /         '.  _.`.   (888    _    |_           _.-''      -\r\n"
					+ "           |           `/    |   \"\"\"   888  / )-..__._.-'      ,/'`-/\r\n"
					+ "           ]     \\    ,:     `.         \"\"  :_/              ,-'  |\r\n"
					+ "            :     \\-_/        `. `a,    ,   :              ,'    /\r\n"
					+ "             `.    Y'       ,_  \\ \"7888\"  ,'   _.--''''---')     |\r\n"
					+ "               \\ .'      _/'  `._\\      ,'---.<...        /     |\r\n"
					+ "               .'      ,' '-.._   ':._,::...,'   /'     ,'      /\r\n"
					+ "              /'     ,/        '`''''           /     ,'       /\r\n"
					+ "             ,'    /  :                        /    ,'       ,-''''._\r\n"
					+ "             |    ()   :                      |    |      .-'        '\r\n"
					+ "             `.   :     ) __............____ .'    |_ .--'\r\n"
					+ "              `.   `.  ,'                   `/       `'-.__\r\n"
					+ "      _,....--'>    : /                     |   __...-._   `\\\r\n"
					+ "   ,-'       .' |   `.                      \"--'        ` ._/'--._\r\n"
					+ " ,'        /'    |   `.                                           ' \r\n"
					+ "/         (.,   /|     :                                            \\.\r\n"
					+ "             `'' :     :\\\r\n" + "                 )     :.:\r\n"
					+ "                 : ; . ; '\r\n" + "                 '_: . '\r\n"
					+ "                   '_:'                                                       ");
		}

		private void printCombat()

		{
			System.out.print("           ▄█▄    ████▄ █▀▄▀█ ███   ██     ▄▄▄▄▀ \n"
					+ "           █▀ ▀▄  █   █ █ █ █ █  █  █ █ ▀▀▀ █    \n"
					+ "           █   ▀  █   █ █ ▄ █ █ ▀ ▄ █▄▄█    █    \n"
					+ "           █▄  ▄▀ ▀████ █   █ █  ▄▀ █  █   █    \n"
					+ "           ▀███▀           █  ███      █  ▀       \n");
		}

		public static void printtrap()

		{
			System.out.println("                           __...------------._\r\n" + 
					"                         ,-'                   `-.\r\n" + 
					"      IT'S A TRAP !   ,-'                         `.\r\n" + 
					"                    ,'                            ,-`.\r\n" + 
					"                   ;                              `-' `.\r\n" + 
					"                  ;                                 .-. \\\r\n" + 
					"                 ;                           .-.    `-'  \\\r\n" + 
					"                ;                            `-'          \\\r\n" + 
					"               ;                                          `.\r\n" + 
					"               ;                                           :\r\n" + 
					"              ;                                            |\r\n" + 
					"             ;                                             ;\r\n" + 
					"            ;                            ___              ;\r\n" + 
					"           ;                        ,-;-','.`.__          |\r\n" + 
					"       _..;                      ,-' ;`,'.`,'.--`.        |\r\n" + 
					"      ///;           ,-'   `. ,-'   ;` ;`,','_.--=:      /\r\n" + 
					"     |'':          ,'        :     ;` ;,;,,-'_.-._`.   ,'\r\n" + 
					"     '  :         ;_.-.      `.    :' ;;;'.ee.    \\|  /\r\n" + 
					"      \\.'    _..-'/8o. `.     :    :! ' ':8888)   || /\r\n" + 
					"       ||`-''    \\\\88o\\ :     :    :! :  :`\"\"'    ;;/\r\n" + 
					"       ||         \\\"88o\\;     `.    \\ `. `.      ;,'\r\n" + 
					"       /)   ___    `.\"'/(--.._ `.    `.`.  `-..-' ;--.\r\n" + 
					"       \\(.=\"\"\"\"\"==.. `'-'     `.|      `-`-..__.-' `. `.\r\n" + 
					"        |          `\"==.__      )                    )  ;\r\n" + 
					"        |   ||           `\"=== '                   .'  .'\r\n" + 
					"        /\\,,||||  | |           \\                .'   .'\r\n" + 
					"        | |||'|' |'|'           \\|             .'   _.' \\\r\n" + 
					"        | |\\' |  |           || ||           .'    .'    \\\r\n" + 
					"        ' | \\ ' |'  .   ``-- `| ||         .'    .'       \\\r\n" + 
					"          '  |  ' |  .    ``-.._ |  ;    .'    .'          `.\r\n" + 
					"       _.--,;`.       .  --  ...._,'   .'    .'              `.__\r\n" + 
					"     ,'  ,';   `.     .   --..__..--'.'    .'                __/_\\\r\n" + 
					"   ,'   ; ;     |    .   --..__.._.'     .'                ,'     `.\r\n" + 
					"  /    ; :     ;     .    -.. _.'     _.'                 /         `\r\n" + 
					" /     :  `-._ |    .    _.--'     _.'                   |\r\n" + 
					"/       `.    `--....--''       _.'                      |\r\n" + 
					"          `._              _..-'                         |\r\n" + 
					"             `-..____...-''                              |\r\n" + 
					"                                                         |\r\n" + 
					"                               mGk                       |");
		}

		public void printfly()

		{
			System.out.print("                ___\r\n" + "                       \\\\ \\\r\n"
					+ "                        \\\\ `\\\r\n" + "     ___                 \\\\  \\\r\n"
					+ "    |    \\                \\\\  `\\\r\n" + "    |_____\\                \\    \\\r\n"
					+ "    |______\\                \\    `\\\r\n" + "    |       \\                \\     \\\r\n"
					+ "    |      __\\__---------------------------------._.\r\n"
					+ "  __|---~~~__o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_o_[][\\__\r\n"
					+ " |___                         /~      )                \\__\r\n"
					+ "     ~~~---..._______________/      ,/_________________/\r\n"
					+ "                            /      /\r\n" + "                           /     ,/\r\n"
					+ "                          /     /\r\n" + "                         /    ,/\r\n"
					+ "                        /    /\r\n" + "                       //  ,/\r\n"
					+ "                      //  /\r\n" + "                     // ,/\r\n" + "");
		}

		public void printisland()

		{
			System.out.print("  ___ __ \r\n" + "   (_  ( . ) )__                  '.    \\   :   /    .'\r\n"
					+ "     '(___(_____)      __           '.   \\  :  /   .'\r\n"
					+ "                     /. _\\            '.  \\ : /  .'\r\n"
					+ "                .--.|/_/__      -----____   _  _____-----\r\n"
					+ "_______________''.--o/___  \\_______________(_)___________\r\n"
					+ "       ~        /.'o|_o  '.|  ~                   ~   ~\r\n"
					+ "  ~            |/    |_|  ~'         ~\r\n"
					+ "               '  ~  |_|        ~       ~     ~     ~\r\n"
					+ "      ~    ~          |_|O  ~                       ~\r\n"
					+ "             ~     ___|_||_____     ~       ~    ~\r\n"
					+ "   ~    ~      .'':. .|_|A:. ..::''.\r\n" + "             /:.  .:::|_|.\\ .:.  :.:\\   ~\r\n"
					+ "  ~         :..:. .:. .::..:  .:  ..:.       ~   ~    ~\r\n"
					+ "             \\.: .:  :. .: ..:: .lcf/\r\n" + "    ~      ~      ~    ~    ~         ~\r\n"
					+ "               ~           ~    ~   ~             ~\r\n"
					+ "        ~         ~            ~   ~                 ~\r\n"
					+ "   ~                  ~    ~ ~                 ~");
		}

		public static void printbottle() {
			System.out.println("              ______\r\n" + "              ( _ _ _)\r\n" + "               |_ _ |\r\n"
					+ "              (_ _ _ )\r\n" + "               |    |\r\n" + "               |____|\r\n"
					+ "               |/  \\|\r\n" + "               (2837)\r\n" + "               |\\__/|\r\n"
					+ "               |    |\r\n" + "               |    |\r\n" + "              / .--. \\\r\n"
					+ "           .-' :(..): `-.\r\n" + "       _.-'    `-..-'    `-._\r\n"
					+ "     .'                      `.\r\n" + "     |___                  ___|\r\n"
					+ "     |   `````--....--'''''   |\r\n" + "     |  |`````--....--'''''|  |\r\n"
					+ "     |  |        $%#       |  |\r\n" + "     |  |        `'\"       |  |\r\n"
					+ "     |  |     FOUNDERS     |  |\r\n" + "     |  |      RESERVE     |  |\r\n"
					+ "     |  |       PORTO      |  |\r\n" + "     |  |                  |  |\r\n"
					+ "     |  |        #-        |  |\r\n" + "     |  |       ###|       |  |\r\n"
					+ "     |  |      .####.      |  |\r\n" + "     |  |      ######      |  |\r\n"
					+ "     |  |      ######      |  |\r\n" + "     |  |        ##        |  |\r\n"
					+ "     |  |  _____####_____  |  |\r\n" + "     |  |  ___SANDEMAN___  |  |\r\n"
					+ "    .'  |                  |  `.\r\n" + "   (  `-.`````--....--'''''.-'  )\r\n"
					+ "    ``-.._``--.._.._..:F_P:.--''\r\n" + "          ``--.._  _..--''\r\n"
					+ "                 `'\r\n" + "");
		}
		
		public static void printCoco()
		{
			System.out.println("             `     ,                  \r\n" + 
					"                                      `.    ,,                  \r\n" + 
					"                                   .  ..  `,..                  \r\n" + 
					"                                  ,: `..  ,,.`                  \r\n" + 
					"                                 `,. ;,. ,,,.                   \r\n" + 
					"                              ,` ,.. ,,,':,.`    .              \r\n" + 
					"                              ,,`:,..,,,:,,,  .:,`              \r\n" + 
					"                              `:,;,,,:,+:,, `;::,               \r\n" + 
					"                        ;:,::, ::'::':';:, ;:,:..               \r\n" + 
					"                    ;;;';::;;,:;;:+;+:'::.+;::,,                \r\n" + 
					"                ':,,,,,,:,,::::;''+;''';''::,;,+::,             \r\n" + 
					"               ::,,,...,,,:,::;'''+#',.` `:+':,:                \r\n" + 
					"               :';,,.,,...,,::++++#```` ````,:';:               \r\n" + 
					"              `:+'.,,.,,,.,,:;'#'+``    `    ```:+`             \r\n" + 
					"              ,;;:,:.,,,,,.:::;+''``` ,..,,. `   `+:            \r\n" + 
					"              ,:':,:,,,,,,,:,'+;'`` `,.```....``  `'            \r\n" + 
					"              :;:.,,:,,,,,,::'''+`  `.`````````. `  #           \r\n" + 
					"             `:';,::;,.,,,::''''#`  ````````````..   ;          \r\n" + 
					"             `;';;,:+,:::,:;',;+#   ``` ````````..` `:          \r\n" + 
					"              ;;;';;:,;::':''::'#   ```` ` ```````` ` :         \r\n" + 
					"              ,;+':;,;:;';:'';''#   `   ` ``````` `   :         \r\n" + 
					"              `;';;:;,;;;:;+,:::'#    ``     `````` `.;         \r\n" + 
					"               ,;+:;':'';;''.::':+;`` ```    ````` ` #:         \r\n" + 
					"                ';;++''+'';+,;:'''+.    ` `` ````    #.         \r\n" + 
					"           .,,,..''''+;'''+';:;:;''+ `    ``````   ` +          \r\n" + 
					"          ..,,,,,:;;+;+''++''::::;'';`    `      `` #;          \r\n" + 
					"            ,,,:,,;':';''++++::':';;+#,   `      ``#;           \r\n" + 
					"            `,.;;;;++#'''+';+;:;;;':+;#####   `` :++;           \r\n" + 
					"          .,:,,:;:'#+'+++++++#:;:;::;+;:+''++''+#++:            \r\n" + 
					"        ...,,,,,,+'';''###+#+'+,;;:;;:'''+++;+#+'';`            \r\n" + 
					"      `....,,,,;;';:;:;++';';;,,;;;:;';;''+;'';'`             \r\n" + 
					"            `,;::;:,:,'';:+':,      ;;;'+'+:               \r\n" + 
					"            :,,,:,,:.',':.,,,                                   \r\n" + 
					"          ,.,.,.... ;,,,`,..                                    \r\n" + 
					"           ````    `.,.. `            This is pizza             \r\n" + 
					"                   ...                                          \r\n" + 
					"                  `.                                            \r\n" + 
					"                                                                ");
		}
	}
}
