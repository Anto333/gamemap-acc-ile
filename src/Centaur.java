class Centaur extends Enemy
{
    Centaur()
    {
        super("Zephyr", 160, 60);
        Attack[] attacks = {
                new Attack("Calin !", 15, 3),
                new Attack("Run run run", 20, 5),
                new Attack("Fist of the blue sky", 30, 10),
                new Attack("The hunt", 40, 30)};
        setAttacks(attacks);
    }
    
    @Override
    public void printskin()
    {
        System.out.println("                           )\r\n" +
                           "                                                    (\\/,\\\r\n" +
                           "                                           ___,      )/(|\r\n" +
                           "                                          /,===      \\\\//\r\n" +
                           "                                       ,==( \"|\"      (# )\r\n" +
                           "                                      ,==='\\_-/       :|\r\n" +
                           "                                       ,---'  \\---.   ||\r\n" +
                           "                                      (     - -    )  |:\r\n" +
                           "                                      |  \\_. '  _./\\ ,'/\\\r\n" +
                           "                                      |  )       / ,-||\\/\r\n" +
                           "                            ___       ( < \\     (\\___/||\r\n" +
                           "                           /   \\,----._\\ \\(   '  )    #;\r\n" +
                           "                          (   /         \\|'',, ,'\\\r\n" +
                           "                          )   |          )\\   '   |\r\n" +
                           "                          (  (|     ,    \\_)      |\r\n" +
                           "                           )  )\\     \\-.__\\   |_, /\r\n" +
                           "                           ( (  \\    )  )  ]  |  (\r\n" +
                           "                            ) ) _) _/ _/   /, )) /\r\n" +
                           "                            (/  \\ <\\ \\      \\ |\\ |\r\n" +
                           "                             ) ._) \\__\\_    ) | )(\r\n" +
                           "                                )_,,\\ )_\\    )|<,_\\\r\n" +
                           "                                   )_\\      /_(  |_\\\r\n" +
                           "                                             )_\\");
    }
}
