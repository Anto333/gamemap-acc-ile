
public class Coord {
	
	protected int[] coord;
	
	public Coord(int i, int j)
	{
		int[] coord = {i, j};
		this.coord = coord;
	}
	
	public void setCoord(int i, int j)
	{
		coord[0] = i;
		coord[1] = j;
	}
	
	public void moveRight()
	{
		coord[1] = coord[1] + 1;
	}
	
	public void moveLeft()
	{
		coord[1] = coord[1] - 1;
	}
	
	public void moveUp()
	{
		coord[0] = coord[0] - 1;
	}
	
	public void moveDown()
	{
		coord[0] = coord[0] + 1;
	}
	
	public int getLat()
	{
		return coord[0];
	}
	
	public int getLong()
	{
		return coord[1];
	}
	
	public Coord getCoord()
	{
		int i = coord[0];
		int j = coord[1];
		Coord coord = new Coord(i, j);
		return coord;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if (!(other instanceof Coord)) {
	        return false;
	    }

	    Coord coord2 = (Coord) other;
		
		boolean equals = false;
		if(this.coord[0] == coord2.getLat() && this.coord[1] == coord2.getLong())
		{
			equals = true;
		}
		return equals;
	}
	
	
	
}
